---
-- @module _

-- Number
-- _.random

return function(_)
  --- Clamps a number within [lower, upper]
  --
  -- @param number the value to clamp
  -- @param lower lower clamp bound
  -- @param upper upper clamp bound
  -- @usage _.clamp(1.2, 0.0, 1.0) == 1.0
  -- @usage _.clamp(1.2, 1.0) == 1.0
  -- @usage _.clamp(-1.2, 1.0) == 0.0
  _.clamp = function(number, lower, upper)
    -- If upper is not specified, then the `lower` variable is the upper bound
    -- and there is no lower bound
    if upper == nil then
      upper = lower
      lower = nil
    end

    if lower ~= nil and number < lower then
      return lower
    end

    if number > upper then
      return upper
    end

    return number
  end


  --- Checks if a number is between start and up to but not including end
  -- If end is not speicifed it is set to start, and start is set to 0
  -- If the start is greated than the end, the parameters are swapped
  _.inRange = function(number, start, _end)
    start = start or 0

    if _end == nil then
      _end = start
      start = 0
    end

    if start > _end then
      _end, start = start, _end
    end

    return number >= start and number < _end
  end
end