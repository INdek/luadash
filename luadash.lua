--- Lodash like library for lua
-- @module _
--
--
-- Differences from lodash:
--
-- * Where possible, we use 1 based indexing (functions like nth).
-- * Functions like _.map, _.forEach iterate both on numerical indexes, and key indexes (i.e. use pairs). We have variants of these such as _.mapIndex, which only use Indexes
-- * We have a different set of predicate functions (_.isNil, _.isTable, etc...)

--
-- Array
-- _.compact
-- _.difference
-- _.differenceBy
-- _.differenceWith
-- _.drop
-- _.dropRight
-- _.dropRightWhile
-- _.dropWhile
-- _.findIndex
-- _.findLastIndex
-- _.flattenDeep
-- _.flattenDepth
-- _.fromPairs
-- _.indexOf
-- _.initial
-- _.intersection
-- _.intersectionBy
-- _.intersectionWith
-- _.join
-- _.last
-- _.lastIndexOf
-- _.pull
-- _.pullAll
-- _.pullAllBy
-- _.pullAllWith
-- _.pullAt
-- _.remove
-- _.reverse
-- _.slice
-- _.sortedIndex
-- _.sortedIndexBy
-- _.sortedIndexOf
-- _.sortedLastIndex
-- _.sortedLastIndexBy
-- _.sortedLastIndexOf
-- _.sortedUniq
-- _.sortedUniqBy
-- _.takeRight
-- _.takeRightWhile
-- _.takeWhile
-- _.union
-- _.unionBy
-- _.unionWith
-- _.uniq
-- _.uniqBy
-- _.uniqWith
-- _.unzip
-- _.unzipWith
-- _.without
-- _.xor
-- _.xorBy
-- _.xorWith
-- _.zip
-- _.zipObject
-- _.zipObjectDeep
-- _.zipWith
--
-- Collection
-- _.countBy
-- _.each -> forEach
-- _.eachRight -> forEachRight
-- _.every
-- _.find
-- _.findLast_
-- _.flatMapDeep
-- _.flatMapDepth
-- _.forEachRight
-- _.groupBy
-- _.invokeMap
-- _.keyBy
-- _.orderBy
-- _.reduceRight
-- _.reject
-- _.sample
-- _.sampleSize
-- _.shuffle
-- _.size
-- _.some
-- _.sortBy
--
-- Date
-- _.now
--
-- Object
-- _.assignIn
-- _.assignInWith
-- _.assignWith
-- _.at
-- _.create
-- _.defaults
-- _.defaultsDeep
-- _.entries -> toPairs
-- _.entriesIn -> toPairsIn
-- _.extend -> assignIn
-- _.extendWith -> assignInWith
-- _.findKey
-- _.findLastKey
-- _.forIn
-- _.forInRight
-- _.forOwn
-- _.forOwnRight
-- _.functions
-- _.functionsIn
-- _.get
-- _.has
-- _.hasIn
-- _.invert
-- _.invertBy
-- _.invoke
-- _.keys
-- _.keysIn
-- _.mapKeys
-- _.merge
-- _.mergeWith
-- _.omit
-- _.omitBy
-- _.pick
-- _.pickBy
-- _.result
-- _.set
-- _.setWith
-- _.toPairs
-- _.toPairsIn
-- _.transform
-- _.unset
-- _.update
-- _.updateWith
-- _.values
-- _.valuesIn
--
-- Properties
-- _.VERSION
-- _.templateSettings
-- _.templateSettings.escape
-- _.templateSettings.evaluate
-- _.templateSettings.imports
-- _.templateSettings.interpolate
-- _.templateSettings.variable
--
-- Methods
-- _.templateSettings.imports._

local _ = setmetatable({
  _VERSION = 'luadash 0.4.0',
  _URL     = 'https://gitlab.com/INdek/luadash',
  _DESCRIPTION = 'lodash like library for lua',
  _LICENSE = [[
    Copyright 2020 Afonso Bordado <afonsobordado@az8.co>

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
    OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
    CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
    TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
    SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  ]]
}, {
  uniqueIdValue = 0,
})


local currPath = (...):match("(.-)[^%.]+$")
local helpers = require(currPath .. 'helpers')(_)
require(currPath .. 'predicates')(_, helpers)
require(currPath .. 'util')(_, helpers)
require(currPath .. 'number')(_, helpers)
require(currPath .. 'seq')(_, helpers)
require(currPath .. '_math')(_, helpers)
require(currPath .. '_string')(_, helpers)
require(currPath .. 'function')(_, helpers)
require(currPath .. 'lang')(_, helpers)


--
-- Collection
--

--- Creates an array of values by running each element in collection thru iteratee. The iteratee is invoked with three arguments: (value, index|key, collection).
--
-- @param collection The collection to iterate over.
-- @param iteratee The function invoked per iteration.
-- @return Returns the new mapped table.
-- @usage function square(n)
--   return n * n
-- end
--
-- _.map({4, 8}, square)
-- -- => {16, 64}
--
-- _.map({ a = 4, b = 8 }, square)
-- -- => {16, 64} (iteration order is not guaranteed)
_.map = function(collection, iteratee)
  if _.isNil(collection) then
    return {}
  end

  iteratee = helpers.getIteratee(iteratee)
  local ret = {}

  for k, value in pairs(collection) do
    table.insert(ret, iteratee(value, k, collection))
  end

  return ret
end



--- Creates an object with the same keys as object and values generated by running each own enumerable string keyed property of object thru iteratee. The iteratee is invoked with three arguments: (value, key, object).
--
-- @param object The table to iterate over.
-- @param iteratee The function invoked per iteration.
-- @return Returns the new mapped table.
-- @usage local users = {
--   fred =    { user = fred,    age = 40 },
--   pebbles = { user = pebbles, age = 1 }
-- }
--
-- _.mapValues(users, function(o) return o.age end)
-- -- => { fred = 40, pebbles = 1 } (iteration order is not guaranteed)
--
-- -- The `_.property` iteratee shorthand.
-- _.mapValues(users, 'age')
-- -- => { fred = 40, pebbles = 1 } (iteration order is not guaranteed)
_.mapValues = function(object, iteratee)
  if _.isNil(object) then
    return {}
  end

  iteratee = helpers.getIteratee(iteratee)
  local ret = {}

  for k, value in pairs(object) do
    ret[k] = iteratee(value, k, object)
  end

  return ret
end


_.flatMap = function(iteratee, cb)
  return _.flatten(_.map(iteratee, cb))
end

-- Flattens a table a single level deep.
_.flatten = function(iteratee)
  if _.isNil(iteratee) then
    return {}
  end

  return _.reduce(iteratee, function(acc, val)
    if _.isTable(val) then
      _.forEach(val, function(val)
        table.insert(acc, val)
      end)
    else
      table.insert(acc, val)
    end

    return acc
  end, {})
end

_.forEach = function(iteratee, cb)
  for i, instance in pairs(iteratee) do
    cb(instance, i)
  end
end

_.reduce = function(iteratee, cb, start)
  local acc = start or nil

  for i, instance in pairs(iteratee) do
    acc = cb(acc, instance, i)
  end

  return acc
end

_.assign = function(base, ...)
  local ret = base or {}
  local args = {...}

  for i, assignArg in ipairs(args) do
    for k, v in pairs(assignArg) do
      ret[k] = v
    end
  end

  return ret
end

_.chunk = function(array, size)
  size = size or 1
  local ret = {}

  for i, v in ipairs(array) do
    if (i-1) % size == 0 then
      table.insert(ret, {})
    end

    table.insert(ret[#ret], v)
  end

  return ret
end


_.filter = function(array, predicate)
  return _.reduce(array, function(acc, val, i)
    local result = predicate(val, i)
    if result then
      table.insert(acc, val)
    end
    return acc
  end, {})
end


-- Fills elements of array with value from start up to, but not including, end.
_.fill = function(table, value, start, _end)
  start = start or 1
  _end = _end or (#table + 1)

  for i = start,(_end-1) do
    table[i] = value
  end

  return table
end

-- Creates a new table by concatenating the input tables
_.concat = function(...)
  local args = {...}
  local ret = {}

  for _i, arg in ipairs(args) do
    if _.isTable(arg) then
      for _i, v in ipairs(arg) do
        table.insert(ret, v)
      end
    else
      table.insert(ret, arg)
    end

  end

  return ret
end

-- Returns the nth arg in table, 1 indexed
-- negative indexes return from the end of the table
_.nth = function(tbl, arg)
  if arg < 0 then
    arg = #tbl + (arg + 1)
  end

  return tbl[arg]
end

_.head = function(tbl)
  return (tbl or {})[1]
end
_.first = _.head


-- Returns a table with all but the first element
--
-- Mutates the input inplace
_.tail = function(tbl)
  table.remove(tbl, 1)
  return tbl
end


--- Creates a slice of `array` with `n` elements taken from the beginning.
--
-- @param array The table to retrieve the elements from.
-- @param n The number of elements to take.
-- @return Returns the slice of `array`.
-- @usage
-- _.take({1, 2, 3}) -- => {1}
-- _.take({1, 2, 3}, 2) -- => {1, 2}
_.take = function(array, n)
  local ret = {}
  for i = 1,n do
    table.insert(ret, array[i])
  end
  return ret
end

--- Checks if value is in collection. If collection is a string, it's checked for a substring of value, otherwise SameValueZero is used for equality comparisons. If fromIndex is negative, it's used as the offset from the end of collection.
--
-- @param collection The collection to inspect.
-- @param value The value to search for.
-- @param fromIndex The index to search from.
-- @return Returns true if value is found, else false.
-- @usage
-- _.includes({1, 2, 3}, 1)
-- -- => true
--
-- _.includes({1, 2, 3}, 1, 3)
-- -- => false
--
-- _.includes({ a = 1, b = 2 }, 1)
-- -- => true
--
-- _.includes('abcd', 'bc')
-- -- => true
_.includes = function(collection, value, fromIndex)
  if _.isString(collection) then
    fromIndex = fromIndex or 1
    return string.find(collection, value, fromIndex) ~= nil
  end

  if _.isNil(fromIndex) then
    for k,v in pairs(collection) do
      if v == value then
        return true
      end
    end
  else
    for k,v in ipairs(collection) do
      if k >= fromIndex and v == value then
        return true
      end
    end
  end

  return false
end

return _
