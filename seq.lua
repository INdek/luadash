---
-- @module _

-- Seq
-- _
-- _.prototype[Symbol.iterator]
-- _.prototype.at
-- _.prototype.chain
-- _.prototype.commit
-- _.prototype.next
-- _.prototype.plant
-- _.prototype.reverse
-- _.prototype.toJSON -> value
-- _.prototype.value
-- _.prototype.valueOf -> value


return function(_, helpers)
  -- TODO: Should we cache results?
  _.chain = function(init)
    local chain = setmetatable({}, {
      __index = function(self, key)
        local fn = _[key]
        if fn == nil then
          error("Unkown function ", key)
        end

        return function(...)
          local mt = getmetatable(self)
          table.insert(mt.chain, {fn, ...})
          return self
        end
      end,
      chain = {}
    })

    -- If we assign this function here, we can access the metatable
    chain.value = function()
      local mt = getmetatable(chain)
      return _.reduce(mt.chain, function(acc, val)
        local fn = _.head(val)
        local args = _.tail(val)

        return fn(acc, helpers.unpack(args))
      end, init)
    end

    return chain
  end


  --- This method invokes `interceptor` and returns `value`. The interceptor is invoked with one argument; (value). The purpose of this method is to "tap into" a method chain sequence in order to modify intermediate results.
  --
  -- @param value the value to provide to `interceptor`
  -- @param interceptor the function to invoke
  -- @return returns `value`
  -- @usage _.chain({1,2,3})
  --  .tap(function(val)
  --    table.insert(val, 4)
  --  end)
  --  .value() -- => {1,2,3,4}
  _.tap = function(value, interceptor)
    interceptor(value)
    return value
  end

  --- This method is like @{_.tap} except that it returns the result of `interceptor`. The purpose of this method is to "pass thru" values replacing intermediate results in a method chain sequence.
  --
  -- @param value the value to provide to `interceptor`
  -- @param interceptor the function to invoke
  -- @return returns `value`
  -- @usage _.chain({1,2,3})
  --  .thru(function(val)
  --    table.insert(val, 4)
  --    return nil
  --  end)
  --  .value() -- => nil
  _.thru = function(value, interceptor)
    return interceptor(value)
  end
end