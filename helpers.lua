return function(_)
  return {
    WHITESPACE_CHARS = ' \t\n\v\f\r',

    _NaN = 0/0,
    _Inf = 1/0,

    -- TODO: Should we expose this in _.unpack?
    unpack = unpack or table.unpack,

    -- TODO: is this equivalent to _.iteratee?
    getIteratee = function (iteratee)
      if _.isString(iteratee) then
        return _.property(iteratee)
      end

      if iteratee == nil then
        return _.identity
      end

      return iteratee
    end,
  }
end