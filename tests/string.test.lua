local _ = require("luadash")

describe("split", function()
  it("returns empty string when given no args", function()
    assert.are.same({''}, _.split())
  end)

  it("returns input when passed nil separator", function()
    assert.are.same({'asd asd'}, _.split('asd asd'))
  end)

  it("splits by separator", function()
    assert.are.same({'a', 'b', 'c'}, _.split('a-b-c', '-'))
  end)

  it("respects limit", function()
    assert.are.same({'a', 'b'}, _.split('a.b.c', '.', 2))
  end)
end)


describe("trimStart", function()
  it("trims the start chars", function()
    assert.are.same('bc', _.trimStart('abc', 'a'))
  end)

  it("defaults to whitespace", function()
    assert.are.same('bc', _.trimStart('\t bc'))
  end)

  it("does not trim the end", function()
    assert.are.same('abcd', _.trimStart('abcd', 'd'))
  end)

  it("trims multiple chars", function()
    assert.are.same('cd', _.trimStart('abcd', 'ba'))
  end)
end)

describe("trimEnd", function()
  it("trims the end chars", function()
    assert.are.same('ab', _.trimEnd('abc', 'c'))
  end)

  it("defaults to whitespace", function()
    assert.are.same('bc', _.trimEnd('bc \t'))
  end)

  it("does not trim the start", function()
    assert.are.same('abcd', _.trimEnd('abcd', 'a'))
  end)

  it("trims multiple chars", function()
    assert.are.same('ab', _.trimEnd('abcd', 'dc'))
  end)
end)

describe("trim", function()
  it("trims both start and end chars", function()
    assert.are.same('b', _.trim('abc', 'ac'))
  end)

  it("defaults to whitespace", function()
    assert.are.same('bc', _.trim('   bc \t'))
  end)

  it("trims multiple chars", function()
    assert.are.same('bc', _.trim('abcd', 'ad'))
  end)
end)