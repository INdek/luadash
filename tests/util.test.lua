local _ = require('luadash')


describe("identity", function()
  it("returns same number", function()
    assert.are.same(_.identity(1), 1)
    assert.are.same(_.identity(-1), -1)
  end)

  it("returns same table", function()
    assert.are.same(_.identity({ 1 }), { 1 })
    assert.are.same(_.identity({ a = 'a' }), { a = 'a' })
  end)
end)

describe("noop", function()
  it("returns nil", function()
    assert.is_nil(_.noop())
  end)
end)

-- TODO: Test with nil args, https://coldfix.de/2017/03/02/lua-argument-packing/
describe("nthArg", function()
  it("returns first", function()
    assert.are.same(1, _.nthArg(1)(1, 2, 3, 4))
  end)

  it("returns last argument when passed -1", function()
    assert.are.same(4, _.nthArg(-1)(1, 2, 3, 4))
  end)

  it("returns nil when called with no args", function()
    assert.is_nil(_.nthArg(1)())
  end)
end)

describe("toPath", function()
  it("returns array when passed an array", function()
    assert.are.same({}, _.toPath({}))
  end)

  it("returns empty table when called without args", function()
    assert.are.same({}, _.toPath())
  end)

  it("returns single element on simple paths", function()
    assert.are.same({'a'}, _.toPath('a'))
  end)

  it("splits dotted paths", function()
    assert.are.same({'a', 'b'}, _.toPath('a.b'))
  end)

  it("converts array indexes into number strings", function()
    assert.are.same({'1'}, _.toPath('[1]'))
  end)

  it("works on mixed array and key access", function()
    assert.are.same({'a', 'b', '1'}, _.toPath('a.b[1]'))
  end)
end)

describe("property", function()
  it("returns whatever when passed empty path", function()
    assert.are.same({}, _.property()({}))
  end)

  it("accepts path as a table", function()
    assert.are.same(1, _.property({'a', 'b'})({
      a = {
        b = 1
      }
    }))
  end)

  it("accesses simple keys", function()
    assert.are.same(1, _.property('a')({
      a = 1
    }))
  end)

  it("accesses nested keys", function()
    assert.are.same(2, _.property('a.b')({
      a = {
        b = 2
      }
    }))
  end)

  it("accesses array like paths", function()
    assert.are.same(2, _.property('a[1]')({
      a = {2}
    }))
  end)

  it("accesses array like paths from end", function()
    assert.are.same(3, _.property('a[-1]')({
      a = {2, 3}
    }))
  end)

  it("returns nil when key not found", function()
    assert.are.same(nil, _.property('a')({}))
  end)

  it("returns nil when nested key not found", function()
    assert.are.same(nil, _.property('a.b.c.d.e')({}))
  end)

  it("returns nil when array index not found", function()
    assert.are.same(nil, _.property('[1]')({}))
  end)

  it("does not crash when passed a number", function()
    assert.are.same(nil, _.property('a')(3))
  end)

end)


describe("constant", function()
  it("returns constant", function()
    assert.are.same(2, _.constant(2)())
  end)
end)

describe("uniqueId", function()
  it("returns different ids", function()
    local a = _.uniqueId()
    local b = _.uniqueId()
    assert.are_not.same(a, b)
  end)

  it("ids are prefixed", function()
    local a = _.uniqueId('contact_')
    assert.are.same(string.sub(a, 1, 8), 'contact_')
  end)
end)