local _ = require('luadash')

describe("chain", function()
  it("is lazy", function()
    local inc = 0
    local chain = _.chain({1,2,3})
      .map(function(v)
        inc = inc + 1
        return v + 1
      end)

    assert.are.same(inc, 0)
    assert.are.same({2,3,4}, chain.value())
    assert.are.same(inc, 3)
  end)

  it("executes operation in the same order that they were defined", function()
    local str = ""
    _.chain({1})
      .map(function(v)
        str = 'a'
        return v
      end)
      .map(function(v)
        str = 'b'
        return v
      end)
      .value()

    assert.are.same(str, "b")
  end)


  it("empty chain returns the same value", function()
    assert.are.same(
      {1,2,3},
      _.chain({1,2,3}).value()
    )
  end)


  describe("tap", function()
    it("mutates the chain value inplace", function()
      assert.are.same(
        {1,2,3,4},
        _.chain({1,2,3})
         .tap(function(val)
           table.insert( val, 4)
         end)
         .value()
      )
    end)
  end)

  describe("thru", function()
    it("mutates the chain value inplace", function()
      assert.are.same(
        nil,
        _.chain({1,2,3})
         .thru(function(val)
           table.insert(val, 4)
           return nil
         end)
         .value()
      )
    end)
  end)
end)