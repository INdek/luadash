local _ = require("luadash")

describe("once", function()
  it("does not call the function on creation", function()
    local s = spy.new(function() end)
    local onced = _.once(s)

    assert.spy(s).was.not_called()
  end)

  it("calls the function on first call", function()
    local s = spy.new(function() end)

    _.once(s)()

    assert.spy(s).was.called(1)
  end)

  it("calls the function once", function()
    local s = spy.new(function()
      return nil
    end)
    local onced = _.once(s)

    onced()
    onced()
    onced()
    onced()

    assert.spy(s).was.called(1)
  end)

  it("returns the same value", function()
    local s = spy.new(function()
      return 1
    end)
    local onced = _.once(s)
    local val1 = onced()
    local val2 = onced()

    assert.spy(s).was.called(1)
    assert.are.equal(val1, val2)
  end)

  it("calls func with args", function()
    local s = spy.new(function(a)
      return a + 2
    end)
    local onced = _.once(s)
    local val1 = onced(3)
    local val2 = onced(888)

    assert.spy(s).was.called(1)
    assert.spy(s).was.called_with(3)
    assert.are.equal(val1, val2)
  end)
end)



describe("ary", function()
  it("does not call the function on creation", function()
    local s = spy.new(function() end)
    _.ary(s, 1)
    assert.spy(s).was.not_called()
  end)

  it("limits the arguments passed into the inner function", function()
    local s = spy.new(function() end)
    _.ary(s, 1)(1, 2)
    assert.spy(s).was.called_with(1)
  end)
end)

describe("unary", function()
  it("limits the arguments passed into the inner function", function()
    local s = spy.new(function() end)
    _.unary(s, 1)(1, 2)
    assert.spy(s).was.called_with(1)
  end)
end)

describe("negate", function()
  it("inverts the predicate", function()
    local s = spy.new(_.isString)
    local res = _.negate(s)("")
    assert.spy(s).was.called_with("")
    assert.are.same(res, false)
  end)
end)