local _ = require('luadash')


describe("clone", function()
  it("clones numbers", function()
    assert.are.same(1, _.clone(1))
  end)

  -- it("clones functions", function()
  --   local original = function() end
  --   local cloned = _.clone(original)

  --   assert.are.same(original, cloned)
  --   assert.are_not.equal(original, cloned)
  -- end)

  it("returns equivalent table", function()
    local original = {
      'a',
      'b',
      a = 1
    }
    local cloned = _.clone(original)

    assert.are.same(original, cloned)
    assert.are_not.equal(original, cloned)
  end)

  it("preserves metatables", function()
    local original = setmetatable({'a', 'b'}, { a = 1 })
    local cloned = _.clone(original)

    local clonedMt = getmetatable(cloned)

    assert.are.same(1, clonedMt.a)
  end)

  it("preserves metatable when no metatable is set", function()
    local original = {'a', 'b'}
    local cloned = _.clone(original)
    local clonedMt = getmetatable(cloned)

    assert.is_true(_.isNil(clonedMt))
  end)
end)


describe("cloneDeep", function()
  it("returns equivalent table", function()
    local original = {
      'a',
      'b',
      a = 1
    }
    local cloned = _.cloneDeep(original)

    assert.are.same(original, cloned)
    assert.are_not.equal(original, cloned)
  end)

  it("preserves metatables", function()
    local original = setmetatable({'a', 'b'}, { a = 1 })
    local cloned = _.cloneDeep(original)

    local clonedMt = getmetatable(cloned)

    assert.are.same(1, clonedMt.a)
  end)

  it("returns equivalent table", function()
    local original = {
      'a',
      'b',
      a = 1
    }
    local cloned = _.cloneDeep(original)

    assert.are.same(original, cloned)
    assert.are_not.equal(original, cloned)
  end)

  it("deep clones", function()
    local original = {
      { a = 1 },
      { b = 2 }
    }
    local cloned = _.cloneDeep(original)

    assert.are.same(original, cloned)
    assert.are_not.equal(original[1], cloned[1])
    assert.are_not.equal(original[2], cloned[2])
  end)


  it("deep clones metatable", function()
    local mt = {
      { a = 1 },
      { b = 2 }
    }
    local original = setmetatable({}, mt)

    local cloned = _.cloneDeep(original)
    local clonedMt = getmetatable(cloned)

    assert.are.same(original, cloned)
    assert.are.same(mt, clonedMt)
    assert.are_not.equal(mt[1], clonedMt[1])
    assert.are_not.equal(mt[2], clonedMt[2])
  end)
end)