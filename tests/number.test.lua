local _ = require("luadash")

describe("clamp", function()
  it("2 args positive", function()
    assert.are.same(_.clamp(1.2, 0.0, 1.0), 1.0)
  end)

  it("2 args negative", function()
    assert.are.same(_.clamp(-1.2, 0.0, 1.0), 0.0)
  end)

  it("1 arg positive", function()
    assert.are.same(_.clamp(99.2, 5.0), 5.0)
  end)

  it("1 arg negative", function()
    assert.are.same(_.clamp(-1.2, 1.0), -1.2)
  end)

  it("within bounds", function()
    assert.are.same(_.clamp(0.7, -0.5, 1.0), 0.7)
  end)
end)

describe("inRange", function()
  it("2 args", function()
    assert.is_true(_.inRange(1.0, 2.0))
    assert.is_false(_.inRange(-1.0, 2.0))
  end)

  it("3 args", function()
    assert.is_true(_.inRange(-0.5, -1.0, 2.0))
    assert.is_false(_.inRange(1.0, 2.0, 3.0))
  end)

  it("not including end", function()
    assert.is_false(_.inRange(3.0, 2.0, 3.0))
  end)

  it("Start greater than end", function()
    assert.is_true(_.inRange(-3, -2, -6))
  end)
end)