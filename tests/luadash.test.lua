local _ = require("luadash")
-- TODO: Make sure we don't change the global environment variables

local function multiplyBy2(val)
  return val * 2
end


describe("map", function()
  it("works", function()
    local start = { 1, 2, 3 }
    local expected = { 2, 4, 6 }
    local mapped = _.map(start, multiplyBy2)

    assert.are.same(mapped, expected)
  end)

  it("returns empty table when passed nil", function()
    assert.are.same({}, _.map(nil))
  end)

  it("passes index as second argument", function()
    local start = { 2, 4, 6 }
    local expected =  { 1, 2, 3 }
    local mapped = _.map(start, function(a, i)
      return i
    end)

    assert.are.same(mapped, expected)
  end)


  it("uses _.identity as the default iteratee", function()
    assert.are.same({1,2,3}, _.map({1,2,3}))
  end)

  it("uses _.proprety as the default iteratee", function()
    assert.are.same({1,2,3}, _.map({ {a = 1}, {a = 2}, {a = 3} }, 'a'))
  end)

  it("iterates on keys", function()
    local start = {
      a = 1,
      b = 2,
      c = 3
    }
    local mapped = _.map(start, multiplyBy2)

    assert.is_true(_.includes(mapped, 2))
    assert.is_true(_.includes(mapped, 4))
    assert.is_true(_.includes(mapped, 6))
  end)
end)



describe("mapValues", function()
  it("works on indexed tables", function()
    local start = { 1, 2, 3 }
    local expected = { 2, 4, 6 }
    local mapped = _.mapValues(start, multiplyBy2)

    assert.are.same(mapped, expected)
  end)

  it("returns empty table when passed nil", function()
    assert.are.same({}, _.mapValues(nil))
  end)

  it("uses _.identity as the default iteratee", function()
    assert.are.same({1,2,3}, _.mapValues({1,2,3}))
  end)

  it("uses _.proprety as the default iteratee", function()
    assert.are.same({1,2,3}, _.mapValues({ {a = 1}, {a = 2}, {a = 3} }, 'a'))
  end)

  it("iterates on keys", function()
    local start = {
      a = 1,
      b = 2,
      c = 3
    }
    local mapped = _.mapValues(start, multiplyBy2)

    assert.are.same(mapped.a, 2)
    assert.are.same(mapped.b, 4)
    assert.are.same(mapped.c, 6)
  end)
end)



describe("flatMap", function()
  it("returns empty table when passed nil", function()
    assert.are.same({}, _.flatMap(nil))
  end)

  it("works", function()
    assert.are.same(
      {1, 2, 3},
      _.flatMap({1, 2, 3}, function(val)
        return {val}
      end)
    )
  end)

  it("flattens up to 1 level", function()
    assert.are.same(
      {{1}, {2}, {3}},
      _.flatMap({1, 2, 3}, function(val)
        return {{val}}
      end)
    )
  end)
end)

describe("flatten", function()
  it("returns empty table when passed nil", function()
    assert.are.same({}, _.flatten(nil))
  end)

  it("works", function()
    assert.are.same(
      {1, 2, 3},
      _.flatten({{1}, {2}, {3}})
    )
  end)

  it("flattens up to 1 level", function()
    assert.are.same(
      {{1}, {2}, {3}},
      _.flatten({{{1}}, {{2}}, {{3}}})
    )
  end)
end)

describe("forEach", function()
  it("iterates an array", function()
    local start = { 1, 2, 3 }
    local expected = { 2, 4, 6 }

    _.forEach(start, function(a, i)
      start[i] = start[i] + a
    end)

    assert.are.same(expected, start)
  end)

  it("works on tables", function()
    local table = {
      a = 1,
      b = 2,
      c = 3
    }
    _.forEach(table, function(v, k)
      table[k] = table[k] + v
    end)

    assert.are.same(table.a, 2)
    assert.are.same(table.b, 4)
    assert.are.same(table.c, 6)
  end)
end)

describe("reduce", function()
  it("defaults to nil", function()
    local reduced = _.reduce({}, function(acc, val)
      return acc + val
    end)

    assert.is_nil(reduced)
  end)


  it("works", function()
    local start = { 1, 2, 3 }
    local expected = 6
    local reduced = _.reduce(start, function(acc, val)
      return acc + val
    end, 0)

    assert.are.same(reduced, expected)
  end)

  it("works on tables", function()
    local table = {
      a = 1,
      b = 2,
      c = 3
    }
    local reduced = _.reduce(table, function(acc, v, k)
      acc[k] = v + v
      return acc
    end, {})

    assert.are.same(reduced.a, 2)
    assert.are.same(reduced.b, 4)
    assert.are.same(reduced.c, 6)
  end)
end)


describe("assign", function()
  it("returns empty table when passed nil", function()
    assert.are.same({}, _.assign(nil))
  end)

  it("merges arrays", function()
    assert.are.same(
      _.assign({1, 2, 3}, {2, 4}),
      {2, 4, 3}
    )
  end)

  it("merges tables", function()
    assert.are.same(
      _.assign({ x = 1 }, { y = 1 }),
      { x = 1, y = 1}
    )
  end)

  it("takes any arguments", function()
    assert.are.same(
      _.assign({}, {}, {}, {}, {}, {}, {}, {}, {1}),
      {1}
    )
  end)
end)

describe("chunk", function()
  it("divides equaly", function()
    assert.are.same(
      _.chunk({1, 2, 3, 4}, 2),
      { {1, 2}, { 3, 4 } }
    )
  end)

  it("fills remainder", function()
    assert.are.same(
      _.chunk({1, 2, 3 }, 2),
      { {1, 2}, { 3 } }
    )
  end)

  it("chunk 3", function()
    local array = { 1, 2, 3, 4, 5, 6, 7, 8, 9 }
    local expected = {
      { 1, 2, 3 },
      { 4, 5, 6 },
      { 7, 8, 9 },
    }
    assert.are.same(_.chunk(array, 3), expected)
  end)
end)

describe("filter", function()
  it("filters according to predicate", function()
    local filtered = _.filter({1,2,3,4}, function(v)
      return v % 2 == 0
    end)
    assert.are.same(filtered, {2,4})
  end)
end)

describe("fill", function()
  it("fills array", function()
    assert.are.same({'a','a','a','a'}, _.fill({1,2,3,4}, 'a'))
  end)

  it("fills array from start", function()
    assert.are.same({1,'a','a','a'}, _.fill({1,2,3,4}, 'a', 2))
  end)

  it("fills array to end", function()
    assert.are.same({'a', 2, 3, 4}, _.fill({1,2,3,4}, 'a', 1, 2))
  end)

  it("does not crash on empty array", function()
    assert.are.same({}, _.fill({}, 'a'))
  end)
end)

describe("concat", function()
  it("returns empty table on empty inputs", function()
    assert.are.same({}, _.concat())
  end)

  it("concats 2 tables", function()
    assert.are.same({1,2}, _.concat({1}, {2}))
  end)

  it("concatenates non arrays into an array", function()
    assert.are.same({1, 2}, _.concat(1, 2))
    assert.are.same({"a", "b"}, _.concat("a", "b"))
  end)
end)



describe("nth", function()
  it("nil on 0 index", function()
    assert.is_nil(_.nth({5,6,7}, 0))
  end)

  it("nil on out of bounds index", function()
    assert.is_nil(_.nth({5,6,7}, 9))
  end)

  it("Pulls from the beginning of the table", function()
    assert.are.same(6, _.nth({5,6,7}, 2))
  end)

  it("Pulls from the end of the table", function()
    assert.are.same(7, _.nth({5,6,7}, -1))
  end)
end)

describe("tail", function()
  it("empty table", function()
    assert.are.same({}, _.tail({}))
  end)

  it("tail 1 element", function()
    assert.are.same({}, _.tail({1}))
  end)

  it("tail 2 elements", function()
    assert.are.same({2}, _.tail({1, 2}))
  end)

  it("tail 3 elements", function()
    assert.are.same({2, 3}, _.tail({1, 2, 3}))
  end)
end)


describe("head", function()
  it("returns nil on empty table", function()
    assert.is_nil(_.head({}))
  end)

  it("returns nil when called with no arguments", function()
    assert.is_nil(_.head())
  end)

  it("gets the first argument of table", function()
    assert.are.same(1, _.head({1}))
  end)
end)

describe("first", function()
  it("is the same function as _.head", function()
    assert.are.equals(_.head, _.first)
  end)
end)

describe("take", function()
  it("gets the first n elements", function()
    assert.are.same({1, 2, 3}, _.take({1, 2, 3, 4, 5}, 3))
  end)
end)

describe("includes", function()
  it("works on indexed tables", function()
    assert.is_true(_.includes({1,2,3}, 3))
  end)

  it("works on keyed tables", function()
    assert.is_true(_.includes({
      a = 1,
      b = 2,
      c = 3
    }, 3))
  end)

  it("starts at index", function()
    assert.is_false(_.includes({1, 2, 3}, 1, 3))
    assert.is_true(_.includes({1, 2, 3}, 3, 2))
  end)

  it("searches strings", function()
    assert.is_true(_.includes('abcd', 'bc'))
  end)
end)