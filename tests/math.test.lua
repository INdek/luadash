local _ = require("luadash")

describe("add", function()
  it("1 + 2 = 3", function()
    assert.are.same(3, _.add(1, 2))
  end)

  it("should work with only one argument", function()
    assert.are.same(1, _.add(1))
    assert.are.same(1, _.add(1, nil))
    assert.are.same(1, _.add(nil, 1))
  end)

  it("returns 0 when called with no args", function()
    assert.are.same(0, _.add())
  end)
end)

describe("subtract", function()
  it("2 - 1 = 1", function()
    assert.are.same(1, _.subtract(2, 1))
    assert.are.same(-1, _.subtract(1, 2))
  end)

  it("should work with only one argument", function()
    assert.are.same(1, _.subtract(1))
    assert.are.same(1, _.subtract(1, nil))
    assert.are.same(1, _.subtract(nil, 1))
  end)

  it("returns 0 when called with no args", function()
    assert.are.same(0, _.subtract())
  end)
end)

describe("multiply", function()
  it("2 * 2 = 4", function()
    assert.are.same(4, _.multiply(2, 2))
    assert.are.same(-4, _.multiply(2, -2))
  end)

  it("should work with only one argument", function()
    assert.are.same(1, _.multiply(1))
    assert.are.same(1, _.multiply(1, nil))
    assert.are.same(1, _.multiply(nil, 1))
  end)

  it("returns 1 when called with no args", function()
    assert.are.same(1, _.multiply())
  end)
end)

describe("divide", function()
  it("2 / 2 = 1", function()
    assert.are.same(1, _.divide(2, 2))
    assert.are.same(-1, _.divide(2, -2))
  end)

  it("should work with only one argument", function()
    assert.are.same(1, _.divide(1))
    assert.are.same(1, _.divide(1, nil))
    assert.are.same(1, _.divide(nil, 1))
  end)

  it("returns 1 when called with no args", function()
    assert.are.same(1, _.divide())
  end)
end)


describe("max", function()
  it("should return nil when called with no arguments", function()
    assert.is_nil(_.max())
  end)

  it("should return nil on empty table", function()
    assert.is_nil(_.max({}))
  end)

  it("returns the maximum of input table", function()
    assert.are.same(5, _.max({1,2,3,4,5}))
  end)

  it("returns the maximum with only negative numbers in the input table", function()
    assert.are.same(-1, _.max({-1,-2,-3,-4,-5}))
  end)
end)

describe("maxBy", function()
  it("should return nil when called with no arguments", function()
    assert.is_nil(_.maxBy())
  end)

  it("should return nil on empty table", function()
    assert.is_nil(_.maxBy({}))
  end)

  it("returns the maximum of input table", function()
    assert.are.same(5, _.maxBy({1,2,3,4,5}, _.identity))
  end)

  it("defaults to _.identity when no mapFn is passed", function()
    assert.are.same(5, _.maxBy({1,2,3,4,5}))
  end)

  it("defaults to _.property when mapFn is a string", function()
    assert.are.same(3, _.maxBy({
      { n = 1 },
      { n = 2 },
      { n = 3 },
    }, 'n'))
  end)
end)


describe("min", function()
  it("should return nil when called with no arguments", function()
    assert.is_nil(_.min())
  end)

  it("should return nil on empty table", function()
    assert.is_nil(_.min({}))
  end)

  it("returns the minimum of input table", function()
    assert.are.same(1, _.min({1,2,3,4,5}))
  end)

  it("returns the minimum with only negative numbers in the input table", function()
    assert.are.same(1, _.min({1,2,3,4,5}))
  end)
end)


describe("minBy", function()
  it("should return nil when called with no arguments", function()
    assert.is_nil(_.minBy())
  end)

  it("should return nil on empty table", function()
    assert.is_nil(_.minBy({}))
  end)

  it("returns the minimum of input table", function()
    assert.are.same(1, _.minBy({1,2,3,4,5}, _.identity))
  end)

  it("defaults to _.identity when no mapFn is passed", function()
    assert.are.same(1, _.minBy({1,2,3,4,5}))
  end)

  it("defaults to _.property when mapFn is a string", function()
    assert.are.same(1, _.minBy({
      { n = 1 },
      { n = 2 },
      { n = 3 },
    }, 'n'))
  end)
end)

describe("sum", function()
  it("should return 0 when called with no arguments", function()
    assert.are.same(0, _.sum())
  end)

  it("should return 0 on empty table", function()
    assert.are.same(0, _.sum({}))
  end)

  it("returns the sum of input table", function()
    assert.are.same(15, _.sum({1,2,3,4,5}))
  end)
end)

describe("sumBy", function()
  it("should return 0 when called with no arguments", function()
    assert.are.same(0, _.sumBy())
  end)

  it("should return 0 on empty table", function()
    assert.are.same(0, _.sumBy({}))
  end)

  it("returns the sum of input table", function()
    assert.are.same(15, _.sumBy({1,2,3,4,5}, _.identity))
  end)

  it("defaults to _.identity as a mapFn", function()
    assert.are.same(15, _.sumBy({1,2,3,4,5}))
  end)

  it("defaults to _.property as a mapFn when passed a string", function()
    assert.are.same(6, _.sumBy({
      { a = 1 },
      { a = 2 },
      { a = 3 },
    }, 'a'))
  end)
end)



describe("mean", function()
  it("should return NaN when called with no arguments", function()
    assert.is_true(_.isNaN(_.mean()))
  end)

  it("should return NaN on empty table", function()
    assert.is_true(_.isNaN(_.mean({})))
  end)

  it("returns the mean of input table", function()
    assert.are.same(5, _.mean({4, 2, 8, 6}))
  end)
end)

describe("meanBy", function()
  it("should return NaN when called with no arguments", function()
    assert.is_true(_.isNaN(_.mean()))
  end)

  it("should return NaN on empty table", function()
    assert.is_true(_.isNaN(_.mean({})))
  end)

  it("returns the mean of input table", function()
    assert.are.same(5, _.mean({4, 2, 8, 6}, _.identity))
  end)

  it("defaults to _.identity as a mapFn", function()
    assert.are.same(5, _.meanBy({4, 2, 8, 6}))
  end)

  it("defaults to _.property as a mapFn when passed a string", function()
    assert.are.same(2, _.meanBy({
      { a = 1 },
      { a = 2 },
      { a = 3 },
    }, 'a'))
  end)
end)


describe("round", function()
  it("should round to decimal if no precision is specified", function()
    assert.are.same(4, _.round(4.006))
  end)

  it("should round to specified precision", function()
    assert.are.same(4.01, _.round(4.006, 2))
  end)

  it("accepts negative precisions", function()
    assert.are.same(4100, _.round(4060, -2))
  end)
end)


describe("ceil", function()
  it("should round to decimal if no precision is specified", function()
    assert.are.same(5, _.ceil(4.006))
  end)

  it("should round to specified precision", function()
    assert.are.same(6.01, _.ceil(6.004, 2))
  end)

  it("accepts negative precisions", function()
    assert.are.same(6100, _.ceil(6040, -2))
  end)
end)


describe("floor", function()
  it("should round to decimal if no precision is specified", function()
    assert.are.same(4, _.floor(4.006))
  end)

  it("should round to specified precision", function()
    assert.are.same(0.04, _.floor(0.046, 2))
  end)

  it("accepts negative precisions", function()
    assert.are.same(4000, _.floor(4060, -2))
  end)
end)
