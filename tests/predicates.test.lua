local _ = require("luadash")

describe("isNaN", function()
  it("returns true for NaN", function()
    assert.is_true(_.isNaN(0/0))
  end)

  it("returns false for nil", function()
    assert.is_false(_.isNaN(nil))
  end)

  it("returns false for empty table", function()
    assert.is_false(_.isNaN({}))
  end)

  it("returns false for 0", function()
    assert.is_false(_.isNaN(0))
  end)

  it("returns false for inf", function()
    assert.is_false(_.isNaN(math.huge))
  end)
end)


describe("isInfinite", function()
  it("returns false for NaN", function()
    assert.is_false(_.isInfinite(0/0))
  end)

  it("returns false for nil", function()
    assert.is_false(_.isInfinite(nil))
  end)

  it("returns false for empty table", function()
    assert.is_false(_.isInfinite({}))
  end)

  it("returns false for 0", function()
    assert.is_false(_.isInfinite(0))
  end)

  it("returns true for inf", function()
    assert.is_true(_.isInfinite(math.huge))
  end)

  it("returns true for -inf", function()
    assert.is_true(_.isInfinite(-(math.huge)))
  end)
end)

describe("isNil", function()
  it("returns false for NaN", function()
    assert.is_false(_.isNil(0/0))
  end)

  it("returns true for nil", function()
    assert.is_true(_.isNil(nil))
  end)

  it("returns false for empty table", function()
    assert.is_false(_.isNil({}))
  end)

  it("returns false for 0", function()
    assert.is_false(_.isNil(0))
  end)
end)

describe("isBoolean", function()
  it("returns false for NaN", function()
    assert.is_false(_.isBoolean(0/0))
  end)

  it("returns false for nil", function()
    assert.is_false(_.isBoolean(nil))
  end)

  it("returns false for empty table", function()
    assert.is_false(_.isBoolean({}))
  end)

  it("returns false for 0", function()
    assert.is_false(_.isBoolean(0))
  end)

  it("returns true for bools", function()
    assert.is_true(_.isBoolean(true))
    assert.is_true(_.isBoolean(false))
  end)
end)


describe("isTable", function()
  it("returns true for tables", function()
    assert.is_true(_.isTable({}))
  end)

  it("returns false for numbers", function()
    assert.is_false(_.isTable(0))
  end)

  it("returns false for strings", function()
    assert.is_false(_.isTable("test"))
  end)

  it("returns false for functions", function()
    assert.is_false(_.isTable(function() end))
  end)
end)

describe("isFunction", function()
  it("returns false for tables", function()
    assert.is_false(_.isFunction({}))
  end)

  it("returns true for functions", function()
    assert.is_true(_.isFunction(function() end))
  end)

  it("returns false for tables with __call metamethod", function()
    local fn = setmetatable({}, {
      __call = function() end
    })
    assert.is_false(_.isFunction(fn))
  end)
end)

describe("isFunctionLike", function()
  it("returns false for tables", function()
    assert.is_false(_.isFunctionLike({}))
  end)

  it("returns true for functions", function()
    assert.is_true(_.isFunctionLike(function() end))
  end)

  it("returns false for tables with __call metamethod", function()
    local fn = setmetatable({}, {
      __call = function() end
    })
    assert.is_true(_.isFunctionLike(fn))
  end)
end)


describe("isNumber", function()
  it("returns true for numbers", function()
    assert.is_true(_.isNumber(0))
    assert.is_true(_.isNumber(1))
    assert.is_true(_.isNumber(100000))
  end)

  it("returns true for inf", function()
    assert.is_true(_.isNumber(math.huge))
    assert.is_true(_.isNumber(-(math.huge)))
  end)


  it("returns true for NaN", function()
    assert.is_true(_.isNumber(0/0))
  end)

  it("returns false for tables", function()
    assert.is_false(_.isNumber({}))
  end)

  it("returns false for strings", function()
    assert.is_false(_.isNumber('0'))
  end)
end)

describe("isFinite", function()
  it("returns true for numbers", function()
    assert.is_true(_.isFinite(0))
    assert.is_true(_.isFinite(1))
    assert.is_true(_.isFinite(100000))
  end)

  it("returns false for inf", function()
    assert.is_false(_.isFinite(math.huge))
    assert.is_false(_.isFinite(-(math.huge)))
  end)


  it("returns false for NaN", function()
    assert.is_false(_.isFinite(0/0))
  end)

  it("returns false for tables", function()
    assert.is_false(_.isFinite({}))
  end)

  it("returns false for strings", function()
    assert.is_false(_.isFinite('0'))
  end)
end)

describe("isInteger", function()
  it("returns true for integers", function()
    assert.is_true(_.isInteger(0))
    assert.is_true(_.isInteger(1))
    assert.is_true(_.isInteger(1.0))
  end)

  it("returns false for floats", function()
    assert.is_false(_.isInteger(1.2))
    assert.is_false(_.isInteger(1.5))
  end)


  it("returns false for inf", function()
    assert.is_false(_.isInteger(math.huge))
    assert.is_false(_.isInteger(-math.huge))
  end)
end)

describe("isPlainTable", function()
  it("returns true for tables", function()
    assert.is_true(_.isPlainTable({}))
  end)

  it("returns false for tables with metatables", function()
    assert.is_false(_.isPlainTable(setmetatable({}, {})))
  end)
end)

describe("isString", function()
  it("returns true for strings", function()
    assert.is_true(_.isString(''))
  end)

  it("returns false for tables", function()
    assert.is_false(_.isString({}))
  end)
end)

describe("isThread", function()
  it("returns true for coroutines", function()
    assert.is_true(_.isThread(coroutine.create(function() end)))
  end)
end)

describe("isUserData", function()
  if _VERSION == 'Lua 5.1' then
    it("returns true for newproxy", function()
      assert.is_true(_.isUserData(newproxy(true)))
    end)
  end
end)