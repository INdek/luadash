---
-- @module _

-- Function
-- _.after
-- _.before
-- _.bind
-- _.bindKey
-- _.curry
-- _.curryRight
-- _.debounce
-- _.defer
-- _.delay
-- _.flip
-- _.memoize
-- _.overArgs
-- _.partial
-- _.partialRight
-- _.rearg
-- _.rest
-- _.spread
-- _.throttle
-- _.wrap


return function(_, helpers)
  --- Creates a function that is restricted to invoking func once. Repeat calls to the function return the value of the first invocation.
  --
  -- @param func function to restrict
  -- @return the new restricted function
  -- @usage local initialize = _.once(createApplication)
  -- initialize()
  -- initialize()
  -- -- `createApplication` is invoked once
  _.once = function(func)
    return setmetatable({}, {
      hasBeenCalled = false,
      val = nil,
      __call = function(t, ...)
        local mt = getmetatable(t)

        if not mt.hasBeenCalled then
          mt.val = func(helpers.unpack({...}))
          mt.hasBeenCalled = true
        end

        return mt.val
      end
    })
  end

  --- Creates a function that invokes `func`, with up to `n` arguments, ignoring any additional arguments.
  --
  -- @param func The function to cap arguments for.
  -- @param n The arity cap
  -- @return Returns the new capped function.
  -- @usage _.ary(_.add, 1)(1, 2) -- => 1
  _.ary = function(func, n)
    return function(...)
      n = n or 1
      local args = _.take({...}, n)
      return func(helpers.unpack(args))
    end
  end

  --- Creates a function that accepts up to one argument, ignoring any additional arguments.
  --
  -- @param func The function to cap arguments for.
  -- @return Returns the new capped function.
  -- @usage _.unary(_.add)(1, 2) -- => 1
  _.unary = function(func)
    return _.ary(func, 1)
  end

  --- Creates a function that negates the result of the predicate `func`.
  --
  -- @param func The predicate to negate.
  -- @return Returns the new negated function.
  -- @usage _.negate(_.isString)("") -- => false
  _.negate = function(func)
    return function(...)
      return not func(helpers.unpack({...}))
    end
  end
end