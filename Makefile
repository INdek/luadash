.PHONY: test docs

test:
	busted .

coverage:
	busted -c .

docs:
	ldoc .
