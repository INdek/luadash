---
-- @module _


-- TODO: Should we add a native module to support _.isNativeFunction?

-- _.isEmpty
-- _.isEqual
-- _.isEqualWith
-- _.isLength
-- _.isMap
-- _.isMatch
-- _.isMatchWith
-- _.isString


return function(_)
  --- Checks if the value is NaN
  -- @param val The value to check
  -- @return Returns true if the value is NaN, false otherwise
  -- @usage _.isNaN(0/0)  -- => true
  --_.isNaN(0)    -- => false
  _.isNaN = function(val)
    return type(val) == "number" and val ~= val
  end

  --- Checks if the value is Infinite
  -- @param val The value to check
  -- @return Returns true if the value is Infinite, false otherwise
  -- @usage _.isInfinite(1/0)  -- => true
  _.isInfinite = function(val)
    return val == (1/0) or val == -(1/0)
  end

  --- Checks if the value is nil
  -- @param val The value to check
  -- @return Returns true if the value is nil, false otherwise
  -- @usage _.isNil(nil)  -- => true
  _.isNil = function(val)
    return val == nil
  end

  --- Checks if the value is a boolean
  -- @param val The value to check
  -- @return Returns true if the value is a boolean, false otherwise
  -- @usage _.isBoolean(true)   -- => true
  --_.isBoolean(false)  -- => true
  _.isBoolean = function(val)
    return type(val) == 'boolean'
  end

  --- Checks if the value is a table
  -- @param val The value to check
  -- @return Returns true if the value is a table, false otherwise
  -- @usage _.isTable({})   -- => true
  _.isTable = function(val)
    return type(val) == 'table'
  end

  --- Checks if the value is a function.
  -- Returns false for tables with the `__call` metamethod, if you want to check for these, use @{_.isFunctionLike}
  --
  -- @param val The value to check
  -- @return Returns true if the value is a function, false otherwise
  -- @usage _.isFunction(function() end)   -- => true
  _.isFunction = function(val)
    return type(val) == 'function'
  end

  --- Checks if the value is a function, or a table with the `__call` metamethod
  --
  -- @param val The value to check
  -- @return Returns true if the value is callable, false otherwise
  -- @usage _.isFunctionLike(function() end)   -- => true
  -- _.isFunctionLike(setmetatable({}, {
  --   __call = function() end
  -- }))   -- => true
  _.isFunctionLike = function(val)
    local mt = getmetatable(val)
    return _.isFunction(val) or ((mt ~= nil) and _.isFunction(mt.__call))
  end


  --- Checks if the value is a number
  --
  -- Note: To exclude Inf, -Inf, and NaN, which are classified as numbers, use the @{_.isFinite} method.
  -- @param val The value to check
  -- @return Returns true if the value is a number, false otherwise
  -- @usage _.isNumber(0)   -- => true
  _.isNumber = function(val)
    return type(val) == 'number'
  end

  --- Checks if the value is a finite primitive number
  --
  -- @param val The value to check
  -- @return Returns true if the value is a number, false otherwise
  -- @usage _.isFinite(0)   -- => true
  --_.isFinite(math.huge)  -- => false
  _.isFinite = function(val)
    return _.isNumber(val) and not _.isNaN(val) and not _.isInfinite(val)
  end


  --- Checks if the value is an integer
  --
  -- @param val The value to check
  -- @return Returns true if the value is an integer number, false otherwise
  -- @usage _.isInteger(1)   -- => true
  --_.isInteger(1.2)  -- => false
  _.isInteger = function(val)
    return val == math.floor(val) and not _.isInfinite(val)
  end

  --- Checks if the value is a table without a metatable
  --
  -- @param val The value to check
  -- @return Returns true if the value is a table without a metatable, false otherwise
  -- @usage _.isPlainTable({})   -- => true
  --_.isPlainTable(setmetatable({}, {}))  -- => false
  _.isPlainTable = function(val)
    return _.isTable(val) and _.isNil(getmetatable(val))
  end

  --- Checks if the value is a string
  --
  -- @param val The value to check
  -- @return Returns true if the value is a string, false otherwise
  -- @usage _.isString("")   -- => true
  _.isString = function(val)
    return type(val) == 'string'
  end


  --- Checks if the value is a thread
  --
  -- @param val The value to check
  -- @return Returns true if the value is a thread, false otherwise
  -- @usage _.isThread(coroutine.create(function() end))   -- => true
  _.isThread = function(val)
    return type(val) == 'thread'
  end

  --- Checks if the value is a userdata object
  --
  -- @param val The value to check
  -- @return Returns true if the value is a userdata object, false otherwise
  -- @usage _.isUserData(userdataobject)   -- => true
  _.isUserData = function(val)
    return type(val) == 'userdata'
  end
end