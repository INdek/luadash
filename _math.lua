---
-- @module _

return function(_, helpers)
  --- Adds two numbers.
  --
  -- @param left The first number in an addition.
  -- @param right The second number in an addition.
  -- @return Returns the total.
  -- @usage _.add(6, 4) -- => 10
  _.add = function(left, right)
    left = left or 0
    right = right or 0

    return left + right
  end

  --- Subtracts two numbers.
  --
  -- @param left The first number in a subtraction.
  -- @param right The second number in a subtraction.
  -- @return Returns the difference.
  -- @usage _.subtract(6, 4) -- => 2
  _.subtract = function(left, right)
    if left == nil and right == nil then
      return 0
    end

    if left ~= nil and right == nil then
      return left
    end

    if left == nil and right ~= nil then
      return right
    end

    return left - right
  end

  --- Multiplies two numbers.
  --
  -- @param left The first number in a multiplication.
  -- @param right The second number in a multiplication.
  -- @return Returns the product.
  -- @usage _.multiply(6, 4) -- => 24
  _.multiply = function(left, right)
    left = left or 1
    right = right or 1

    return left * right
  end

  --- Divide two numbers.
  --
  -- @param left The first number in a division.
  -- @param right The second number in a division.
  -- @return Returns the quotient.
  -- @usage _.divide(6, 4) -- => 1.5
  _.divide = function(left, right)
    left = left or 1
    right = right or 1

    return left / right
  end


  --- This method is like @{_.max} except that it accepts iteratee which is invoked for each element in array to generate the criterion by which the value is ranked. The iteratee is invoked with one argument: (value).
  --
  -- @param table The array to iterate over.
  -- @param iteratee The second number in a division.
  -- @return Returns the maximum value.
  -- @usage local objects = {
  --   { n = 1 },
  --   { n = 2 }
  -- }
  --
  -- _.maxBy(objects, function(o) return o.n end)
  -- -- => { n = 2 }
  --
  -- -- The `_.property` iteratee shorthand.
  -- _.maxBy(objects, 'n')
  -- -- => { n = 2 }
  _.maxBy = function(table, iteratee)
    iteratee = helpers.getIteratee(iteratee)
    if table == nil or #table == 0 then
      return nil
    end

    return _.reduce(table, function(acc, val)
      val = iteratee(val)

      if acc == nil or val > acc then
        return val
      else
        return acc
      end
    end, nil)
  end

  --- Computes the maximum value of a table. If the table is empty or falsey, nil is returned.
  --
  -- @param table The table to iterate over.
  -- @return Returns the maximum value.
  -- @usage
  -- _.max({4, 2, 8, 6})
  -- -- => 8
  --
  -- _.max({})
  -- -- => nil
  _.max = function(table)
    return _.maxBy(table)
  end

  --- This method is like @{_.min} except that it accepts iteratee which is invoked for each element in array to generate the criterion by which the value is ranked. The iteratee is invoked with one argument: (value).
  --
  -- @param table The array to iterate over.
  -- @param iteratee The iteratee invoked per element.
  -- @return Returns the minimum value.
  -- @usage local objects = {
  --   { n = 1 },
  --   { n = 2 }
  -- }
  --
  -- _.minBy(objects, function(o) return o.n end)
  -- -- => { n = 1 }
  --
  -- -- The `_.property` iteratee shorthand.
  -- _.minBy(objects, 'n')
  -- -- => { n = 1 }
  _.minBy = function(table, iteratee)
    iteratee = helpers.getIteratee(iteratee)
    if table == nil or #table == 0 then
      return nil
    end

    return _.reduce(table, function(acc, val)
      val = iteratee(val)

      if acc == nil or val < acc then
        return val
      else
        return acc
      end
    end, nil)
  end

  --- Computes the minimum value of table.
  --
  -- @param table The table to iterate over.
  -- @return Returns the minimum value.
  -- @usage
  -- _.min({4, 2, 8, 6})
  -- -- => 2
  --
  -- _.min({})
  -- -- => nil
  _.min = function(table)
    return _.minBy(table)
  end


  --- This method is like @{_.sum} except that it accepts iteratee which is invoked for each element in array to generate the value to be summed. The iteratee is invoked with one argument: (value).
  --
  -- @param table The array to iterate over.
  -- @param iteratee The iteratee invoked per element.
  -- @return Returns the minimum value.
  -- @usage local objects = {
  --   { n = 1 },
  --   { n = 2 }
  -- }
  --
  -- _.sumBy(objects, function(o) return o.n end)
  -- -- => 3
  --
  -- -- The `_.property` iteratee shorthand.
  -- _.sumBy(objects, 'n')
  -- -- => 3
  _.sumBy = function(table, iteratee)
    iteratee = helpers.getIteratee(iteratee)

    return _.reduce(table or {}, function(acc, val)
      return _.add(acc, iteratee(val))
    end, 0)
  end

  --- Computes the sum of the values in a `table`.
  --
  -- @param table The table to iterate over.
  -- @return Returns the sum.
  -- @usage _.sum({4, 2, 8, 6}) -- => 20
  _.sum = function(table)
    return _.sumBy(table)
  end

  --- This method is like @{_.mean} except that it accepts iteratee which is invoked for each element in array to generate the value to be averaged. The iteratee is invoked with one argument: (value).
  --
  -- @param table The array to iterate over.
  -- @param iteratee The iteratee invoked per element.
  -- @return Returns the minimum value.
  -- @usage local objects = {
  --   { n = 4 },
  --   { n = 2 },
  --   { n = 8 },
  --   { n = 6 }
  -- }
  --
  -- _.meanBy(objects, function(o) return o.n end)
  -- -- => 5
  --
  -- -- The `_.property` iteratee shorthand.
  -- _.meanBy(objects, 'n')
  -- -- => 5
  _.meanBy = function(table, iteratee)
    iteratee = helpers.getIteratee(iteratee)

    if table == nil then
      return helpers._NaN
    end

    return _.sum(_.map(table, iteratee)) / #table
  end

  --- Computes the mean of the values in a `table`.
  --
  -- @param table The table to iterate over.
  -- @return Returns the mean.
  -- @usage _.mean({4, 2, 8, 6}) -- => 5
  _.mean = function(table)
    return _.meanBy(table)
  end


  --- Computes `number` rounded to `precision`.
  --
  -- @param number The number to round.
  -- @param precision The precision to round to.
  -- @return Returns the rounded number.
  -- @usage _.round(4.006)
  -- -- => 4
  --
  -- _.round(4.006, 2)
  -- -- => 4.01
  --
  -- _.round(4060, -2)
  -- -- => 4100
  _.round = function(number, precision)
    precision = precision or 0
    local mult = 10 ^ precision

    return math.floor(number * mult + 0.5) / mult
  end

  --- Computes a `number` rounded down to `precision`.
  --
  -- @param number The number to round down.
  -- @param precision The precision to round down to.
  -- @return Returns the rounded down number.
  -- @usage
  -- _.floor(4.006)
  -- -- => 4
  --
  -- _.floor(0.046, 2)
  -- -- => 0.04
  --
  -- _.floor(4060, -2)
  -- -- => 4000
  _.floor = function(number, precision)
    precision = precision or 0
    local mult = 10 ^ precision

    return math.floor(number * mult) / mult
  end

  --- Computes a `number` rounded up to `precision`.
  --
  -- @param number The number to round up.
  -- @param precision The precision to round up to.
  -- @return Returns the rounded up number.
  -- @usage
  -- _.ceil(4.006)
  -- -- => 5
  --
  -- _.ceil(6.004, 2)
  -- -- => 6.01
  --
  -- _.ceil(6040, -2)
  -- -- => 6100
  _.ceil = function(number, precision)
    precision = precision or 0
    local mult = 10 ^ precision

    return math.ceil(number * mult) / mult
  end
end