---
-- @module _

-- _.castArray
-- _.cloneDeepWith
-- _.cloneWith
-- _.conformsTo
-- _.eq
-- _.gt
-- _.gte
-- _.lt
-- _.lte
-- _.toArray
-- _.toFinite
-- _.toInteger
-- _.toLength
-- _.toNumber
-- _.toPlainObject
-- _.toSafeInteger
-- _.toString

return function(_, helpers)
  --- Creates a shallow clone of `value`.
  --
  -- @param value The value to clone.
  -- @return Returns the cloned value.
  -- @usage _.clone({1,2,3}) -- => {1,2,3}
  _.clone = function(value)
    if _.isTable(value) then
      local mt = getmetatable(value)
      return setmetatable(
        _.mapValues(value),
        mt and _.mapValues(mt)
      )
    end

    return value
  end

  --- This method is like @{_.clone} except that it recursively clones `value`.
  --
  -- @param value The value to clone.
  -- @return Returns the cloned value.
  -- @usage _.cloneDeep({1,2,3}) -- => {1,2,3}
  _.cloneDeep = function(value)
    if _.isTable(value) then
      local mt = getmetatable(value)

      return setmetatable(
        _.mapValues(value, _.cloneDeep),
        _.cloneDeep(mt)
      )
    end

    return _.clone(value)
  end
end