---
-- @module _


-- Util
-- _.attempt
-- _.bindAll
-- _.cond
-- _.conforms
-- _.constant
-- _.defaultTo
-- _.flow
-- _.flowRight
-- _.identity
-- _.iteratee
-- _.matches
-- _.matchesProperty
-- _.method
-- _.methodOf
-- _.mixin
-- _.noConflict
-- _.over
-- _.overEvery
-- _.overSome
-- _.propertyOf
-- _.range
-- _.rangeRight
-- _.runInContext
-- _.stubArray
-- _.stubFalse
-- _.stubObject
-- _.stubString
-- _.stubTrue
-- _.times

return function(_)
  -- This method returns the first argument it receives.
  _.identity = function(value)
    return value
  end

  -- Returns nil
  _.noop = function(tbl)
    return nil
  end

  -- Creates a function that gets the argument at index n. If n is negative, the nth argument from the end is returned.
  _.nthArg = function(idx)
    idx = idx or 1

    return function(...)
      return _.nth({...}, idx)
    end
  end

  -- Converts value to a property path array
  _.toPath = function(value)
    if value == nil then
      return {}
    end

    if _.isString(value) then
      return _.chain(value)
        .split('.')
        .flatMap(function(val)
          return _.split(val, "[")
        end)
        .map(function(val)
          local inner = string.gsub(val, "%]", "")
          return inner
        end)
        .value()
    end

    return value
  end

  --- Creates a function that returns the value at path of a given object.
  --
  -- @param path The path of the property to get.
  -- @return the new accessor function
  -- @usage local objects = {
  --   { 'a' = { 'b' = 2 } },
  --   { 'a' = { 'b' = 1 } }
  -- }
  --
  -- _.map(objects, _.property('a.b'));
  -- -- => [2, 1]
  _.property = function(path)
    return function(val)
      return _.reduce(_.toPath(path), function(acc, entry)
        if acc == nil then
          return acc
        end

        if not _.isTable(acc) and not _.isUserData(acc) then
          return nil
        end

        if tonumber(entry) ~= nil then
          local num = tonumber(entry)
          if num < 0 then
            return acc[#acc - (num + 1)]
          else
            return acc[num]
          end
        end

        return acc[entry]
      end, val)
    end
  end


  --- Creates a function that returns the value at path of a given object.
  --
  -- @param value The value to return from the new function.
  -- @return Returns the new constant function.
  -- @usage local fn = _.constant(1)
  -- fn() -- => 1
  _.constant = function(value)
    return function()
      return value
    end
  end

  --- Generates a unique ID. If prefix is given, the ID is appended to it.
  --
  -- @param prefix The value to prefix the ID with.
  -- @return Returns the unique ID.
  -- @usage _.uniqueId('contact_');
  -- -- => 'contact_104'
  --
  -- _.uniqueId();
  -- -- => '105'
  _.uniqueId = function(prefix)
    prefix = prefix or ''
    local mt = getmetatable(_)
    local id = mt.uniqueIdValue
    mt.uniqueIdValue = id + 1
    return prefix .. id
  end
end