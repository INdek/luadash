---
-- @module _

-- String
-- _.camelCase
-- _.capitalize
-- _.deburr
-- _.endsWith
-- _.escape
-- _.escapeRegExp
-- _.kebabCase
-- _.lowerCase
-- _.lowerFirst
-- _.pad
-- _.padEnd
-- _.padStart
-- _.parseInt
-- _.repeat
-- _.replace
-- _.snakeCase
-- _.startCase
-- _.startsWith
-- _.template
-- _.toLower
-- _.toUpper
-- _.truncate
-- _.unescape
-- _.upperCase
-- _.upperFirst
-- _.words

return function(_, helpers)
  --- Splits string by separator.
  --
  -- @param input The string to split.
  -- @param separator The separator pattern to split by.
  -- @param limit The length to truncate results to.
  -- @return Returns the string segments.
  -- @usage _.split('a-b-c', '-', 2) -- => {'a', 'b'}
  _.split = function(input, separator, limit)
    input = input or ''
    if separator == nil then
      return {input}
    end

    local pattern = '[^'..separator..']+'

    local res = {}
    local func = function(w)
      if limit == nil or #res < limit then
        table.insert(res, w)
      end
    end

    string.gsub(input, pattern, func)
    return res
  end


  --- Removes leading whitespace or specified characters from string.
  --
  -- @param input The input string
  -- @param chars characters to be trimmed, by default all whitespace characters
  -- @return the trimmed string
  -- @usage _.trimStart('  abc  ') == 'abc  '
  -- @usage _.trimStart('-_-abc-_-', '_-') == 'abc-_-'
  _.trimStart = function(input, chars)
    chars = chars or helpers.WHITESPACE_CHARS
    return string.gsub(input, '^['..chars..']+(.*)$', '%1')
  end

  --- Removes trailing whitespace or specified characters from string.
  --
  -- @param input The input string
  -- @param chars characters to be trimmed, by default all whitespace characters
  -- @return the trimmed string
  -- @usage _.trimEnd('  abc  ') == '  abc'
  -- @usage _.trimEnd('-_-abc-_-', '_-') == '-_-abc'
  _.trimEnd = function(input, chars)
    chars = chars or helpers.WHITESPACE_CHARS

    -- TODO: Reversing and calling trimStart, is not ideal
    -- string.gsub(input, '^(.-)['..chars..']$', '%1')
    return string.reverse(_.trimStart(string.reverse(input), chars))
  end

  --- Removes leading and trailing whitespace or specified characters from string.
  --
  -- @param input The input string
  -- @param chars characters to be trimmed, by default all whitespace characters
  -- @return the trimmed string
  -- @usage _.trim('  abc  ') == 'abc'
  -- @usage _.trim('-_-abc-_-', '_-') == 'abc'
  _.trim = function(input, chars)
    return _.trimStart(_.trimEnd(input, chars), chars)
  end
end